﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotePad
{
    public partial class NotePad : Form
    {
        public NotePad()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPad fp = new FrmPad();
            fp.MdiParent = this;
            fp.Show();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if(result == DialogResult.OK)
            {
                FrmPad fp = new FrmPad();
                fp.MdiParent = this;
                fp.FilePath = openFileDialog1.FileName;
                fp.Show();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.MdiChildren.Length == 0)
            {
                return;
            }

            DialogResult result = saveFileDialog1.ShowDialog();
            if(result == DialogResult.OK)
            {
                FrmPad activePad = (FrmPad)this.ActiveMdiChild;
                activePad.FilePath = saveFileDialog1.FileName;
                activePad.Save();
            }
        }
    }
}
