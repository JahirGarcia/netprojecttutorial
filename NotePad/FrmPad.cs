﻿using NotePad.BaseStream;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotePad
{
    public partial class FrmPad : Form
    {
        private string filePath;
        private SecuentialStream ss;
        public FrmPad()
        {
            InitializeComponent();
        }

        public string FilePath
        {
            get
            {
                return filePath;
            }

            set
            {
                filePath = value;
            }
        }

        private void FrmPad_Load(object sender, EventArgs e)
        {
            if(filePath != null)
            {
                ss = new SecuentialStream(filePath);
                textBox1.Text = ss.ReadText();
            }
        }

        public void Save()
        {
            if(filePath != null)
            {
                ss = new SecuentialStream(filePath);
                ss.WriteText(textBox1.Text);
            }
        }
    }
}
