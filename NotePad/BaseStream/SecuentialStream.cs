﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotePad.BaseStream
{
    class SecuentialStream
    {
        private string filePath;

        public SecuentialStream(string filePath)
        {
            this.filePath = filePath;
        }

        public void WriteText(string text)
        {
            try
            {
                using(StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.Write(text);
                }
            } catch(IOException){}
        }

        public string ReadText()
        {
            string text = "";
            try
            {
                using(StreamReader sr = new StreamReader(filePath))
                {
                    string line;
                    while((line=sr.ReadLine()) != null)
                    {
                        text += line;
                    }
                }
            }
            catch (IOException){}

            return text;
        }
    }
}
