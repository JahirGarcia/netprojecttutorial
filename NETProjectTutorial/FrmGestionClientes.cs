﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionClientes : Form
    {

        private DataSet dsClientes;
        private BindingSource bsClientes;

        public DataSet DsCliente
        {
            set
            {
                dsClientes = value;
            }
        }

        public FrmGestionClientes()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        private void FrmGestionEmpleados_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = dsClientes;
            bsClientes.DataMember = dsClientes.Tables["Cliente"].TableName;

            dgvClientes.DataSource = bsClientes;
            dgvClientes.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.DtClientes = dsClientes.Tables["Cliente"];
            fc.ShowDialog();
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            bsClientes.Filter = string.Format("Cedula like '%{0}%' or Apellidos like '%{0}%'", txtFilter.Text);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataRow row = getSelectedRow();

            if (row != null)
            {
                FrmCliente fc = new FrmCliente();
                fc.DtClientes = dsClientes.Tables["Cliente"];
                fc.DrClientes = row;
                fc.ShowDialog();
            }
        }

        private void brnDelete_Click(object sender, EventArgs e)
        {
            DataRow row = getSelectedRow();

            if (row != null)
            {
                DialogResult result = MessageBox.Show("Seguro que deseas eliminar este cliente?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    dsClientes.Tables["Cliente"].Rows.Remove(row);
                    MessageBox.Show("El cliente ha sido eliminado satisfactoreamente!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private DataRow getSelectedRow()
        {
            DataGridViewSelectedRowCollection rowCollecction = dgvClientes.SelectedRows;

            if (rowCollecction.Count == 0)
            {
                MessageBox.Show("Selecciona primero una fila de la tabla", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }

            DataGridViewRow gridRow = rowCollecction[0];
            DataRowView rowView = (DataRowView)gridRow.DataBoundItem;

            if (rowView != null)
            {
                DataRow row = rowView.Row;
                return row;
            }
            else
            {
                return null;
            }

        }
    }
}
