﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.Dao
{
    interface IDaoCliente : IDao<Cliente>
    {
        Cliente FindById(int id);
        Cliente FindByCedula(string Cedula);
        List<Cliente> FindByApellido(string apellido);
    }
}
