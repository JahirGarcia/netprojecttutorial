﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.Dao
{
    interface IDao<T>
    {
        void Save(T t);
        int Update(T t);
        bool Delete(T t);
        List<T> FindAll();
    }
}
