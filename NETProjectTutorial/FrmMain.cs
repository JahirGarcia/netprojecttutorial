﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using NETProjectTutorial.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];
            ProductoModel.Populate();
            foreach (Producto p in ProductoModel.GetProductos())
            {
                dtProductos.Rows.Add(p.Id, p.Sku, p.Nombre, p.Descripcion, p.Cantidad, p.Precio, p.Sku + " " + p.Nombre);
            }

            EmpleadoModel.Populate();
            foreach(Empleado em in EmpleadoModel.GetListEmpleados())
            {
                dsProductos.Tables["Empleado"].Rows.Add(em.Id, em.Cedula, em.Inss, em.Nombres, em.Apellidos, em.Direccion, em.Tconvencional, em.Tcelular, em.Sexo1, em.Salario, em.Nombres + " " + em.Apellidos);
            }

            foreach(Cliente c in new ClienteModel().GetList()) {
                dsProductos.Tables["Cliente"].Rows.Add(c.Id, c.Cedula, c.Nombres, c.Apellidos, c.Telefono, c.Correo, c.Direccion);
            }
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void nuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsCliente = dsProductos;
            fgc.Show();
        }
    }
}
