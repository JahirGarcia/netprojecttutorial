﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        private string codFactura;
        private DateTime fecha;
        private double subtotal;
        private double iva;
        private double total;
        private int cantidad;
        private double precio;
        private string sku;
        private string nombreProducto;
        private string nombreEmpleado;
        private string apellidoEmpleado;
        private string nombreCliente;
        private string apellidoCliente;

        public string CodFactura
        {
            get
            {
                return codFactura;
            }

            set
            {
                codFactura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string NombreProducto
        {
            get
            {
                return nombreProducto;
            }

            set
            {
                nombreProducto = value;
            }
        }

        public string NombreEmpleado
        {
            get
            {
                return nombreEmpleado;
            }

            set
            {
                nombreEmpleado = value;
            }
        }

        public string ApellidoEmpleado
        {
            get
            {
                return apellidoEmpleado;
            }

            set
            {
                apellidoEmpleado = value;
            }
        }

        public string NombreCliente
        {
            get
            {
                return nombreCliente;
            }

            set
            {
                nombreCliente = value;
            }
        }

        public string ApellidoCliente
        {
            get
            {
                return apellidoCliente;
            }

            set
            {
                apellidoCliente = value;
            }
        }

        public ReporteFactura(string codFactura, DateTime fecha, double subtotal, double iva, double total, int cantidad, double precio, string sku, string nombreProducto, string nombreEmpleado, string apellidoEmpleado, string nombreCliente, string apellidoCliente)
        {
            this.codFactura = codFactura;
            this.fecha = fecha;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
            this.cantidad = cantidad;
            this.precio = precio;
            this.sku = sku;
            this.nombreProducto = nombreProducto;
            this.nombreEmpleado = nombreEmpleado;
            this.apellidoEmpleado = apellidoEmpleado;
            this.nombreCliente = nombreCliente;
            this.apellidoCliente = apellidoCliente;
        }
    }
}
