﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleados : Form
    {

        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;

        public DataSet DsEmpleados
        {
            set
            {
                dsEmpleados = value;
            }
        }

        public FrmGestionEmpleados()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        private void FrmGestionEmpleados_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmEmpleado fe = new FrmEmpleado();
            fe.DtEmpleados = dsEmpleados.Tables["Empleado"];
            fe.ShowDialog();
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            bsEmpleados.Filter = string.Format("Cedula like '%{0}%' or Apellidos like '%{0}%'", txtFilter.Text);
        }
    }
}
