﻿using NETProjectTutorial.entities;
using NETProjectTutorial.Implements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.Models
{
    class ClienteModel
    {
        private DaoImplementsCliente daoCliente;

        public ClienteModel()
        {
            this.daoCliente = new DaoImplementsCliente();
        }

        public List<Cliente> GetList()
        {
            return daoCliente.FindAll();
        }
    }
}
