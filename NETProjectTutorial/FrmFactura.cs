﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {
        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private bool _canUpdate = true;
        private bool _needUpdate = false;
        private double subtotal;
        private double iva;
        private double total;
        private string codFactura;

        public FrmFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }

        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            cmbProductos.DataSource = dsSistema.Tables["Producto"];
            cmbProductos.DisplayMember = "SKUN";
            cmbProductos.ValueMember = "Id";

            cmbClientes.DataSource = dsSistema.Tables["Cliente"];
            cmbClientes.DisplayMember = "NA";
            cmbClientes.ValueMember = "Id";

            dsSistema.Tables["ProductoFactura"].Rows.Clear();
            bsProductoFactura.DataSource = dsSistema.Tables["ProductoFactura"];
            dgvProductoFactura.DataSource = bsProductoFactura;

            codFactura = "FA" + dsSistema.Tables["Factura"].Rows.Count + 1;
            txtCodFactura.Text = codFactura;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();
        }


        //-------------------------------
        private void UpdateData()
        {
            if (cmbEmpleados.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistema.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombres = dataRow.Field<String>("Nombres"),
                        Apellidos = dataRow.Field<String>("Apellidos")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombres.Contains(cmbEmpleados.Text)));
            }
            else
            {
                RestartTimer();
            }
        }


        //Actualizar el combo con nuevos datos
        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbEmpleados.Text;

            if (dataSource.Count() > 0)
            {
                cmbEmpleados.DataSource = dataSource;

                var sText = cmbEmpleados.Items[0].ToString();
                cmbEmpleados.SelectionStart = text.Length;
                cmbEmpleados.SelectionLength = sText.Length - text.Length;
                cmbEmpleados.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleados.DroppedDown = false;
                cmbEmpleados.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        private void cmbEmpleados_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }
        }

        private void CmbEmpleados_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void CmbEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        //---------------------------------

        private void cmbProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow row = ((DataRowView)cmbProductos.SelectedItem).Row;
            Console.WriteLine(cmbProductos.SelectedValue);
            txtCantidad.Text = row["Cantidad"].ToString();
            txtPrecio.Text = row["Precio"].ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow selectedRow = ((DataRowView)cmbProductos.SelectedItem).Row;
                DataRow newRow = dsSistema.Tables["ProductoFactura"].NewRow();
                newRow["Id"] = selectedRow["Id"];
                newRow["SKU"] = selectedRow["SKU"];
                newRow["Nombre"] = selectedRow["Nombre"];
                newRow["Cantidad"] = 1;
                newRow["Precio"] = selectedRow["Precio"];
                dsSistema.Tables["ProductoFactura"].Rows.Add(newRow);
            } catch(ConstraintException)
            {
                MessageBox.Show(this, "Producto ya agregado, verifique por favor!",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvProductoFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgrProductoFactura = dgvProductoFactura.Rows[e.RowIndex];
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura.DataBoundItem).Row;

            DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(drProductoFactura["Id"]);

            if(int.Parse(drProductoFactura["Cantidad"].ToString()) >
                int.Parse(drProducto["Cantidad"].ToString()))
            {
                MessageBox.Show("La cantidad de producto a vender no puede ser mayor que la de almacen", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                drProductoFactura["Cantidad"] = int.Parse(drProducto["Cantidad"].ToString());
            }
            CalcularFactura();
        }

        private void CalcularFactura()
        {
            subtotal = 0;
            foreach(DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                subtotal += int.Parse(dr["Cantidad"].ToString()) * double.Parse(dr["Precio"].ToString());
            }

            iva = subtotal * 0.15;
            total = subtotal + iva;

            txtSubtotal.Text = subtotal.ToString();
            txtIva.Text = iva.ToString();
            txtTotal.Text = total.ToString();
        }

        private void dgvProductoFactura_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CalcularFactura();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection selectedRows = dgvProductoFactura.SelectedRows;

            if(selectedRows.Count == 0)
            {
                MessageBox.Show("Seleccione primero una fila de la tabla", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DataGridViewRow dgvRow = selectedRows[0];
            DataRow drProductoFactura = ((DataRowView)dgvRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show("Seguro que quiere eliminar esta fila", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if(result == DialogResult.Yes)
            {
                dsSistema.Tables["ProductoFactura"].Rows.Remove(drProductoFactura);
            }

        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if(dsSistema.Tables["ProductoFactura"].Rows.Count == 0)
            {
                return;
            }

            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["CodFactura"] = codFactura;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Observaciones"] = txtObservaciones.Text;
            drFactura["Empleado"] = cmbEmpleados.SelectedValue;
            drFactura["Subtotal"] = subtotal;
            drFactura["Iva"] = iva;
            drFactura["Total"] = total;
            drFactura["Cliente"] = cmbClientes.SelectedValue;

            dsSistema.Tables["Factura"].Rows.Add(drFactura);

            foreach(DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsSistema.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Factura"] = drFactura["Id"];
                drDetalleFactura["Producto"] = dr["Id"];
                drDetalleFactura["Cantidad"] = dr["Cantidad"];
                drDetalleFactura["Precio"] = dr["Precio"];
                dsSistema.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);

                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Id"]);
                drProducto["Cantidad"] = double.Parse(drProducto["Cantidad"].ToString()) - double.Parse(dr["Cantidad"].ToString());
            }

            FrmReporteFactura frf = new FrmReporteFactura();
            frf.MdiParent = this.MdiParent;
            frf.DsSistema = dsSistema;
            frf.Show();

            Dispose();
        }
    }
}
