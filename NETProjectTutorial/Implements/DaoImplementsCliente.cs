﻿using NETProjectTutorial.Dao;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;

namespace NETProjectTutorial.Implements
{
    class DaoImplementsCliente : IDaoCliente
    {
        //Header Cliente
        private BinaryReader brHCliente;
        private BinaryWriter bwHCliente;

        //Data Cliente
        private BinaryReader brDCliente;
        private BinaryWriter bwDCliente;

        private FileStream fsHCliente;
        private FileStream fsDCliente;

        private const string FILENAME_HEADER = "hcliente.dat";
        private const string FILENAME_DATA = "dcliente.dat";
        private const int SIZE = 390;

        private void Open()
        {
            try
            {
                fsDCliente = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if(!File.Exists(FILENAME_HEADER))
                {
                    fsHCliente = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    brHCliente = new BinaryReader(fsHCliente);
                    bwHCliente = new BinaryWriter(fsHCliente);

                    brDCliente = new BinaryReader(fsDCliente);
                    bwDCliente = new BinaryWriter(fsDCliente);

                    bwHCliente.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwHCliente.Write(0);//n
                    bwHCliente.Write(0);//k
                } else
                {
                    fsHCliente = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    brHCliente = new BinaryReader(fsHCliente);
                    bwHCliente = new BinaryWriter(fsHCliente);

                    brDCliente = new BinaryReader(fsDCliente);
                    bwDCliente = new BinaryWriter(fsDCliente);
                }
            } catch(IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        private void Close()
        {
            try
            {
                if (brDCliente != null)
                {
                    brDCliente.Close();
                }
                if (bwDCliente != null)
                {
                    bwDCliente.Close();
                }
                if (brHCliente != null)
                {
                    brHCliente.Close();
                }
                if (bwDCliente != null)
                {
                    bwDCliente.Close();
                }
                if (fsDCliente != null)
                {
                    fsDCliente.Close();
                }
                if (fsHCliente != null)
                {
                    fsHCliente.Close();
                }
            } catch(IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public Cliente FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Cliente FindByCedula(string Cedula)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> FindByApellido(string apellido)
        {
            throw new NotImplementedException();
        }

        public void Save(Cliente t)
        {
            Open();
            brHCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHCliente.ReadInt32();
            int k = brHCliente.ReadInt32();

            long dpos = k * SIZE;
            bwDCliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwDCliente.Write(k++);
            bwDCliente.Write(t.Cedula);
            bwDCliente.Write(t.Nombres);
            bwDCliente.Write(t.Apellidos);
            bwDCliente.Write(t.Telefono);
            bwDCliente.Write(t.Correo);
            bwDCliente.Write(t.Direccion);

            bwHCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            bwHCliente.Write(n++);
            bwHCliente.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwHCliente.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwHCliente.Write(k);
            Close();
        }

        public int Update(Cliente t)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Cliente t)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> FindAll()
        {
            Open();
            List<Cliente> clientes = new List<Cliente>();

            brHCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHCliente.ReadInt32();

            for(int i=0; i< n; i++)
            {
                // Calculamos la posicion de la cabecera
                long hpos = 8 + i * 4;
                brHCliente.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brHCliente.ReadInt32();
                // Calculmos la posicion de los datos
                long dpos = (index - 1) * SIZE;
                brDCliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brDCliente.ReadInt32();
                string cedula = brDCliente.ReadString();
                string nombres = brDCliente.ReadString();
                string apellidos = brDCliente.ReadString();
                string telefono = brDCliente.ReadString();
                string correo = brDCliente.ReadString();
                string direccion = brDCliente.ReadString();

                Cliente c = new Cliente(id, cedula, nombres, apellidos, telefono, correo, direccion);
                clientes.Add(c);
            }

            Close();
            return clientes;
        }
    }
}
