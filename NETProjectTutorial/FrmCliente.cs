﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable dtClientes;
        private DataRow drClientes;

        public FrmCliente()
        {
            InitializeComponent();
        }

        public DataTable DtClientes { set { dtClientes = value; } }

        public DataRow DrClientes { set { drClientes = value; } }

        private void btnNew_Click(object sender, EventArgs e)
        {
            string cedula, nombres, apellidos, direccion, tconvencional, correo;

            cedula = msktCedula.Text;
            nombres = txtNombres.Text;
            apellidos = txtApellidos.Text;
            direccion = txtDireccion.Text;
            tconvencional = msktTelConv.Text;
            correo = mskCorreo.Text;

            if(drClientes != null)
            {
                DataRow drNew = dtClientes.NewRow();

                int index = dtClientes.Rows.IndexOf(drClientes);
                drNew["Id"] = drClientes["Id"];
                drNew["Cedula"] = cedula;
                drNew["Nombres"] = nombres;
                drNew["Apellidos"] = apellidos;
                drNew["Telefono"] = tconvencional;
                drNew["Correo"] = correo;
                drNew["Direccion"] = direccion;
                drNew["NA"] = nombres + " " + apellidos;

                dtClientes.Rows.RemoveAt(index);
                dtClientes.Rows.InsertAt(drNew, index);
            } else
            {
                int id = dtClientes.Rows.Count + 1;
                dtClientes.Rows.Add(id, cedula, nombres, apellidos, tconvencional, correo, direccion, nombres+" "+ apellidos);
                MessageBox.Show("El cliente se ha agregado satisfactoreamente!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void setupFields()
        {
            if (drClientes != null)
            {
                msktCedula.Text = drClientes["Cedula"].ToString();
                txtNombres.Text = drClientes["Nombres"].ToString();
                txtApellidos.Text = drClientes["Apellidos"].ToString();
                msktTelConv.Text = drClientes["Telefono"].ToString(); ;
                mskCorreo.Text = drClientes["Correo"].ToString();
                txtDireccion.Text = drClientes["Direccion"].ToString();
            }
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            setupFields();
        }
    }
}
